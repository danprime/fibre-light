#animation class

import time
import sys
import cv2
import _rpi_ws281x as ws
import numpy as np
from random import randrange
from operator import setitem
import datetime

from alibrary import Pixel
from alibrary import Strand
from alibrary import Strandizer
from alibrary import Pixel

RED = 0xFF0000
GREEN = 0x00FF00
BLUE = 0x0000FF
WHITE = 0xFFFFFF
BLACK = 0x000000
now = datetime.datetime.now()

A1 = Strand(0,20,1, "0", RED)
A2 = Strand(39,20,-1, "0", RED)
A3 = Strand(40,20,1, "0", RED)
A4 = Strand(79,20,-1, "0", RED)
A5 = Strand(80,20,1, "0", RED)
A6 = Strand(119,20,-1, "0", RED)
A7 = Strand(120,20,1, "0", RED)
A8 = Strand(159,20,-1, "0", RED)
A9 = Strand(160,20,1, "0", RED)
A10 = Strand(199,20,-1, "0", RED)
A11 = Strand(200,20,1, "0", RED)
A12 = Strand(239,20,-1, "0", RED)
A13 = Strand(240,20,1, "0", RED)
A14 = Strand(279,20,-1, "0", RED)
A15 = Strand(280,20,1, "0", RED)

B1 = Strand(19,20,-1, "0", GREEN)
B2 = Strand(20,20,1, "0", GREEN)
B3 = Strand(59,20,-1, "0", GREEN)
B4 = Strand(60,20,1, "0", GREEN)
B5 = Strand(99,20,-1, "0", GREEN)
B6 = Strand(100,20,1, "0", GREEN)
B7 = Strand(139,20,-1, "0", GREEN)
B8 = Strand(140,20,1, "0", GREEN)
B9 = Strand(179,20,-1, "0", GREEN)
B10 = Strand(180,20,1, "0", GREEN)
B11 = Strand(219,20,-1, "0", GREEN)
B12 = Strand(220,20,1, "0", GREEN)
B13 = Strand(259,20,-1, "0", GREEN)
B14 = Strand(260,20,1, "0", GREEN)
B15 = Strand(299,20,-1, "0", GREEN)

#Column 1 represents Strands 1,6,7,12,13
#So to add column animation
# strandizer = Strandizer()
# strandizer.addStrand(A1)
# strandizer.addStrand(A6)
# strandizer.addStrand(A7)
# strandizer.addStrand(A12)
# strandizer.addStrand(A13)
#Column 2 represents Strands 2,5,8,11,14
#Column 3 represents Strands 3,4,9,10,15

# LED configuration.
LED_PER_PIXEL  = 20         # Leds per section
LED_CHANNEL    = 0
LED_COUNT      = 300        # How many LEDs to light.
LED_FREQ_HZ    = 800000     # Frequency of the LED signal.  Should be 800khz or 400khz.
LED_DMA_NUM    = 5          # DMA channel to use, can be 0-14.
LED_GPIO       = 18         # GPIO connected to the LED signal line.  Must support PWM!
LED_INVERT     = 0          # Set to 1 to invert the LED signal, good if using NPN
              # transistor as a 3.3V->5V level converter.  Keep at 0
              # for a normal/non-inverted signal.

HORI = 160
VERT = 120
TOT_WHITES = HORI*VERT/3
CAL_FACTOR = 0.95
WHITE_LIMIT = TOT_WHITES*CAL_FACTOR
kernel = np.ones((5,5), np.uint8)

#Initializations
strandizer = Strandizer()
animationList = []


def diffImg(t0, t1, t2):
  """Compare image images and returns a bitmap image where white pixels represent movement"""
  d1 = cv2.absdiff(t2, t1)
  d2 = cv2.absdiff(t1, t0)
  return cv2.bitwise_and(d1, d2)

def detectMove(img):
  """Checks if an image has white pixels"""
  retu = False
  numero = np.count_nonzero(img)
  #minV = np.amin(img)
  #if minV==0:
  #print numero, img.shape
  if numero < WHITE_LIMIT:
    retu = True
  return(retu)

def scanRegions(img):
    """ Cheks entire image for movement"""
    img_morph = cv2.morphologyEx(img,cv2.MORPH_OPEN,kernel)
    im1 = img_morph[0:VERT/3,:]
    im2 = img_morph[VERT/3:2*VERT/3,:]
    im3 = img_morph[2*VERT/3:VERT,:]

    return detectMove(im1), detectMove(im2), detectMove(im3)


def animationOrder():
  animationList.append(A1)
  animationList.append(A2)
  animationList.append(A3)
  animationList.append(A4)
  animationList.append(A5)
  animationList.append(A6)
  animationList.append(A7)
  animationList.append(A8)
  animationList.append(A9)
  animationList.append(A10)
  animationList.append(A11)
  animationList.append(A12)
  animationList.append(A13)
  animationList.append(A14)
  animationList.append(A15)

  animationList.append(B1)
  animationList.append(B2)
  animationList.append(B3)
  animationList.append(B4)
  animationList.append(B5)
  animationList.append(B6)
  animationList.append(B7)
  animationList.append(B8)
  animationList.append(B9)
  animationList.append(B10)
  animationList.append(B11)
  animationList.append(B12)
  animationList.append(B13)
  animationList.append(B14)
  animationList.append(B15)

def addAWall():
  strandizer.addStrand(A1)
  strandizer.addStrand(A2)
  strandizer.addStrand(A3)
  strandizer.addStrand(A4)
  strandizer.addStrand(A5)
  strandizer.addStrand(A6)
  strandizer.addStrand(A7)
  strandizer.addStrand(A8)
  strandizer.addStrand(A9)
  strandizer.addStrand(A10)
  strandizer.addStrand(A11)
  strandizer.addStrand(A12)
  strandizer.addStrand(A13)
  strandizer.addStrand(A14)
  strandizer.addStrand(A15)

def addBWall():
  strandizer.addStrand(B1)
  strandizer.addStrand(B2)
  strandizer.addStrand(B3)
  strandizer.addStrand(B4)
  strandizer.addStrand(B5)
  strandizer.addStrand(B6)
  strandizer.addStrand(B7)
  strandizer.addStrand(B8)
  strandizer.addStrand(B9)
  strandizer.addStrand(B10)
  strandizer.addStrand(B11)
  strandizer.addStrand(B12)
  strandizer.addStrand(B13)
  strandizer.addStrand(B14)
  strandizer.addStrand(B15)

def columnA1():
  strandizer.addStrand(Strand(0,20,1, 0, RED))
  strandizer.addStrand(Strand(119,20,-1, 0, RED))
  strandizer.addStrand(Strand(120,20,1, 0, RED))
  strandizer.addStrand(Strand(239,20,-1, 0, RED))
  strandizer.addStrand(Strand(240,20,1, 0, RED))

def columnB1():
  strandizer.addStrand(Strand(19,20,-1, 0, GREEN))
  strandizer.addStrand(Strand(100,20,1, 0, GREEN))
  strandizer.addStrand(Strand(139,20,-1, 0, GREEN))
  strandizer.addStrand(Strand(220,20,1, 0, GREEN))
  strandizer.addStrand(Strand(259,20,-1, 0, GREEN))

def columnA2():
  strandizer.addStrand(Strand(39,20,-1, 0, RED))
  strandizer.addStrand(Strand(80,20,1, 0, RED))
  strandizer.addStrand(Strand(159,20,-1, 0, RED))
  strandizer.addStrand(Strand(200,20,1, 0, RED))
  strandizer.addStrand(Strand(279,20,-1, 0, RED))

def columnB2():
  strandizer.addStrand(Strand(20,20,1, 0, GREEN))
  strandizer.addStrand(Strand(99,20,-1, 0, GREEN))
  strandizer.addStrand(Strand(140,20,1, 0, GREEN))
  strandizer.addStrand(Strand(219,20,-1, 0, GREEN))
  strandizer.addStrand(Strand(260,20,1, 0, GREEN))

def columnA3():
  strandizer.addStrand(Strand(40,20,1, 0, RED))
  strandizer.addStrand(Strand(79,20,-1, 0, RED))
  strandizer.addStrand(Strand(160,20,1, 0, RED))
  strandizer.addStrand(Strand(199,20,-1, 0, RED))
  strandizer.addStrand(Strand(280,20,1, 0, RED))

def columnB3():
  strandizer.addStrand(Strand(59,20,-1, 0, GREEN))
  strandizer.addStrand(Strand(60,20,1, 0, GREEN))
  strandizer.addStrand(Strand(179,20,-1, 0, GREEN))
  strandizer.addStrand(Strand(180,20,1, 0, GREEN))
  strandizer.addStrand(Strand(299,20,-1, 0, GREEN))



def qNextAnimation():
  if (len(animationList) > 0):
    strand = animationList.pop(0)
    strandizer.addStrand(strand)
  else:
    r = randrange(9)
    if (r == 0):
      addAWall()
    elif(r ==1):
      addBWall()
    elif (r == 2):
      addAWall()
      addBWall()
    elif (r == 3):
      columnA1()
    elif (r == 4):
      columnB1()
    elif (r == 5):
      columnA2()
    elif (r == 6):
      columnB2()
    elif (r == 7):
      columnA3()
    elif (r == 8):
      columnB3()

def random_color():
    """Creates a random 24bit color"""
    r = randrange(50)
    g = 155+randrange(100)
    b = 155+randrange(100)
    return int('0x' + hex(r)[2:].zfill(2) + hex(g)[2:].zfill(2) + hex(b)[2:].zfill(2), 16)

def random_red():
    """Creates a random 24bit color"""
    r = 155+randrange(100)
    g = 15
    b = 15
    return int('0x' + hex(r)[2:].zfill(2) + hex(g)[2:].zfill(2) + hex(b)[2:].zfill(2), 16)

def initLED():
  # Create a ws2811_t structure from the LED configuration.
  # Note that this structure will be created on the heap so you need to be careful
  # that you delete its memory by calling delete_ws2811_t when it's not needed.
  leds = ws.new_ws2811_t()

  cam1 = cv2.VideoCapture(0)               # Initialize camera
  cam2 = cv2.VideoCapture(1)               # Initialize camera
  cam1.set(3, HORI)                         # Set horizontal resolution
  cam1.set(4, VERT)                         # Set vertical resolution
  cam2.set(3, HORI)                         # Set horizontal resolution
  cam2.set(4, VERT)                         # Set vertical resolution
  time.sleep(2)
  #cam1.set(15,-8.0)
  t1_minus = cv2.cvtColor(cam1.read()[1], cv2.COLOR_RGB2GRAY)  
  t1 = cv2.cvtColor(cam1.read()[1], cv2.COLOR_RGB2GRAY)
  t1_plus = cv2.cvtColor(cam1.read()[1], cv2.COLOR_RGB2GRAY)
  t2_minus = cv2.cvtColor(cam2.read()[1], cv2.COLOR_RGB2GRAY)  
  t2 = cv2.cvtColor(cam2.read()[1], cv2.COLOR_RGB2GRAY)
  t2_plus = cv2.cvtColor(cam2.read()[1], cv2.COLOR_RGB2GRAY)


  ws.ws2811_t_count_set(leds, LED_COUNT)     # 
  ws.ws2811_t_gpionum_set(leds, LED_GPIO)    # 
  ws.ws2811_t_invert_set(leds, LED_INVERT)   #
  ws.ws2811_t_freq_set(leds, LED_FREQ_HZ)    #
  ws.ws2811_t_dmanum_set(leds, LED_DMA_NUM)  #

  led_data = ws.new_led_data(LED_COUNT)      #

  resp = ws.ws2811_init(leds)
  color = RED
  if resp != 0:
    raise RuntimeError('ws2811_init failed with code {0}'.format(resp))

  LEDS = np.zeros(LED_COUNT)
  animationOrder()

  try:
    offset = 0
    ws.ws2811_t_leds_set(leds,led_data)
    
    while True:
      strandizer.stepAnimate()

      for i in range(LED_COUNT):
        color = random_color();
        if (strandizer.output[i] == 1):
          color = strandizer.colours[i]
        ws.led_data_setitem(led_data, i, color)    
      
      t1_minus = t1
      t1 = t1_plus
      t2_minus = t2
      t2 = t2_plus
      #imgimg = cv2.GaussianBlur(frame, (3,3), 0)
      t1_plus = cv2.cvtColor(cam1.read()[1], cv2.COLOR_RGB2GRAY)
      t2_plus = cv2.cvtColor(cam2.read()[1], cv2.COLOR_RGB2GRAY)
      #print t1_plus.shape, t2_plus.shape
      frame1 = diffImg(t1_minus, t1, t1_plus)
      frame2 = diffImg(t2_minus, t2, t2_plus)
      #print frame1.shape, frame2.shape
      th1 = cv2.adaptiveThreshold(frame1,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,21,8)
      th2 = cv2.adaptiveThreshold(frame2,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,21,8)
      left1, center1, right1 = scanRegions(th1)
      left2, center2, right2 = scanRegions(th2)

      if(left1):
        columnA1()
        #print("A1")

      if(left2):
        columnB1()
        #print("B1")

      if(center1):
        columnA2()
        #print("A2")

      if(center2):
        columnB2()
        #print("B2")

      if(right1):
        columnA3()
        #print("A3")

      if(right2):
        columnB3()
        #print("B3")

      #output  
      resp = ws.ws2811_render(leds)
      
      if resp != 0:
        raise RuntimeError('ws2811_render failed with code {0}'.format(resp))
      
      time.sleep(0.05)
      # Increase offset to animate colors moving.  Will eventually overflow, which
      # is fine.
      offset += 1

  finally:
    # Ensure ws2811_fini is called before the program quits.
    ws.ws2811_fini(leds)
    # Example of calling delete function to clean up structure memory.  Isn't
    # strictly necessary at the end of the program execution here, but is good practice.
    ws.delete_ws2811_t(leds)
    
  # Wrap following code in a try/finally to ensure cleanup functions are called
  # after library is initialized.

initLED()