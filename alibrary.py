#animation class

import time
import sys

#abstraction of a single Pixel (LED) has position and colour
class Pixel:
  def __init__(self, positionIndex, colour):
    self.positionIndex = positionIndex
    #currently defaulting the pixel to have no colour at start to allow for overwriting and simple change detection
    self.colour = ""
    self.changed = False

  def changeColor(self, newColour):
    self.colour = newColour
    self.changed = True
    #print("Changing:"+str(self.positionIndex))

  def toString(self):
    sys.stdout.write(str(self.positionIndex) + ":" + str(self.colour) + "|")

#Abstraction of a single strand of Pixels/LEDs has notion of starting point and direction and transforming colour
class Strand:
  def __init__(self, startIndex, length, direction, initialColour, finishColour):
    self.start = startIndex
    self.direction = direction
    self.startColour = initialColour
    self.endColour = finishColour
    self.finished = False

    self.pixels = []

    self.length = length
    self.end = startIndex + length - 1    
    self.currentPosition = 0

    i = 0

    if (self.direction > 0):
      
      while (i < self.length):      
        self.pixels.append(Pixel(self.start+i, self.startColour))
        i=i+1
    else:
      
      while (i < self.length):      
        self.pixels.append(Pixel(self.start-i, self.startColour))
        i=i+1

  #Step through a single animation. This would be where you would create different animations based on direction
  def stepAnimate(self):
    #Check limits
    if (self.currentPosition >= self.length):
      #print("FORCING as finished")
      self.finished = True

    if (self.currentPosition < 0):
      #print("FORCING as finished")
      self.finished = True

    if (self.finished == False):
      #print("Changing:" + str(self.currentPosition) + "target:" + str(self.end))

      #Simple animation of changing colour - since the pixels are set up in the correct
      #animation order we just iterate through until we're done.
      self.pixels[self.currentPosition].changeColor(self.endColour)

      if (self.currentPosition < self.length):
        self.currentPosition = self.currentPosition + 1
      else:
        #print("Mark as finished (+1)" + str(self.currentPosition)  + "vs " + str(self.end))
        self.finished = True

  def toString(self):
    i = 0
    while (i < self.length):      
      self.pixels[i].toString()
      i=i+1
    sys.stdout.write("\n")

#Strandizer is the manager for animating multiple strands simultaneously
#You can always pull the output and colours
class Strandizer:
  def __init__(self):
    self.strands = []
    self.strandCount = 0
    self.output = [0 for _ in range(300)]
    self.colours = [0 for _ in range(300)]
    self.changed = [0 for _ in range(300)]

  def addStrand(self, newStrand):
    self.strands.append(newStrand)
    self.strandCount = self.strandCount + 1;

  def checkForCleanup(self):
    #Noting which ones should be removed
    self.cleanupStrands = []
    i = 0
    while (i < self.strandCount):
      #remove strand if finished
      if (self.strands[i].finished == True):
        self.cleanupStrands.append(i)
        #print("mark:" + str(i))
      i = i + 1


  def stepAnimate(self):
    i = 0
    while(i < self.strandCount):
      #animate each strand
      #print("Animating:" + str(self.strandCount))
      self.strands[i].stepAnimate();
      i = i + 1

    self.checkForCleanup()
    while(len(self.cleanupStrands) > 0):
      self.strands.pop(self.cleanupStrands[0])
      self.strandCount = self.strandCount - 1
      self.checkForCleanup()

    
    #Update the outputs
    self.CombineStrands()
    #self.toString()


  #Method that performs a "union" of all animations
  #This could be where we detect collisions and make decisions as well
  #This method is automatically called whenever stepAnimate is called.

  def CombineStrands(self):
    i = 0
  
    #initialization of outputs - reset everything to 0
    self.output = [0 for _ in range(300)]
    self.colours = [0 for _ in range(300)]
    self.changed = [0 for _ in range(300)]
  
    #Iterate through each strand to "union" them into the output array
    while(i < self.strandCount):
      
      for pixie in (self.strands[i].pixels):
        #If we do not check for this flag then we overwrite all the pixels in
        #in the strand with the default colour. This leads to shortened animations
        #We want to give existing animations a chance to move across.
        if (pixie.changed == True):

          #This causes the newer animation to be favoured over the older one.
          if ((self.colours[pixie.positionIndex] != 0) or (self.changed[pixie.positionIndex] == 0)): 
            self.colours[pixie.positionIndex] = pixie.colour
            self.output[pixie.positionIndex] = 1        
            self.changed[pixie.positionIndex] = 1
        
      i = i + 1

  def toString(self):
    for dot in (self.output):
      sys.stdout.write(str(dot) + "|")
    sys.stdout.write("\n")

    for dot in (self.colours):
      sys.stdout.write(str(dot) + "|")
    sys.stdout.write("\n")